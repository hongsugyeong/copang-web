import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CardProductInfo from '~/components/card-product-info'
Vue.component('CardProductInfo', CardProductInfo)
